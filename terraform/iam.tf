# aws_iam_role iot_role
resource "aws_iam_role" "iot_role" {
  name = "iot_role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
          "Service": "iot.amazonaws.com"
        },
        "Action": ["sts:AssumeRole"]
      }
    ]
  }
  )
}

# aws_iam_role_policy iam_policy_for_dynamodb
resource "aws_iam_role_policy" "additional_policy_for_dynamodb" {
  name   = "additional_policy_for_dynamodb"
  role   = aws_iam_role.iot_role.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect   = "Allow",
        Action   = [ "dynamodb:PutItem"],
        Resource = "*"
      }
    ]
  })
}

# aws_iam_role_policy iam_policy_for_timestream_writing
resource "aws_iam_role_policy" "timestream_policy" {
  name = "test_policy"
  role = aws_iam_role.iot_role.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "timestream:WriteRecords",
        ]
        Effect   = "Allow"
        Resource = aws_timestreamwrite_table.temperaturesensor.arn
      },
      {
        Action = [
          "timestream:DescribeEndpoints",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}
# aws_iam_role lambda_role
resource "aws_iam_role" "lambda_role" {
    name = "lambda_role"

    # Terraform's "jsonencode" function converts a
    # Terraform expression result to valid JSON syntax.
    assume_role_policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action = "sts:AssumeRole"
          Principal = {
            Service = "lambda.amazonaws.com"
          }
          "Effect": "Allow",
          "Sid": ""
        },
      ]
    })
  }

# aws_iam_role_policy iam_policy_for_timestream_reading for Lambda
resource "aws_iam_policy" "timestream_policy" {
  name        = "timestream_policy"
  description = "IAM policy for Timestream"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect   = "Allow",
        Action   = ["timestream:Select"],
        Resource = aws_timestreamwrite_table.temperaturesensor.arn
      },
      {
        Effect   = "Allow",
        Action   = ["timestream:DescribeEndpoints"],
        Resource = "*"
      }
    ]
  })
}
# aws_iam_role_policy iam_policy_for_iot_publishing for Lambda
resource "aws_iam_policy" "iot_publish_policy" {
  name        = "iot_publish_policy"
  description = "IAM policy for IoT Publish"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect   = "Allow",
        Action   = ["iot:Publish"],
        Resource = "*"
      }
    ]
  })
}



###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iam_role_policy" "iam_policy_for_logs" {
#  name = "cloudwatch_policy"
#  role = aws_iam_role.iot_role.id
#
#  policy = <<EOF
#{
#        "Version": "2012-10-17",
#        "Statement": [
#            {
#                "Effect": "Allow",
#                "Action": [
#                    "logs:CreateLogGroup",
#                    "logs:CreateLogStream",
#                    "logs:PutLogEvents",
#                    "logs:PutMetricFilter",
#                    "logs:PutRetentionPolicy"
#                 ],
#                "Resource": [
#                    "*"
#                ]
#            }
#        ]
#    }
#EOF
#}


###########################################################################################
# Enable the following resources to enable logging for your Lambda function (helps debug)
###########################################################################################

#resource "aws_cloudwatch_log_group" "example" {
#  name              = "/aws/lambda/${aws_lambda_function.ac_control_lambda.function_name}"
#  retention_in_days = 14
#}
#
#resource "aws_iam_policy" "lambda_logging" {
#  name        = "lambda_logging"
#  path        = "/"
#  description = "IAM policy for logging from a lambda"
#
#  policy = <<EOF
#{
#  "Version": "2012-10-17",
#  "Statement": [
#    {
#      "Action": [
#        "logs:CreateLogGroup",
#        "logs:CreateLogStream",
#        "logs:PutLogEvents"
#      ],
#      "Resource": "arn:aws:logs:*:*:*",
#      "Effect": "Allow"
#    }
#  ]
#}
#EOF
#}
#
#resource "aws_iam_role_policy_attachment" "lambda_logs" {
#  role       = aws_iam_role.lambda_role.name
#  policy_arn = aws_iam_policy.lambda_logging.arn
#}
