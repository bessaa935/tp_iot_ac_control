# aws_iot_certificate cert
# aws_iot_certificate
resource "aws_iot_certificate" "cert" {
  active = true
}
# aws_iot_policy
resource "aws_iot_policy" "pubsub" {
  name = "PubSubToAnyTopic"
  policy = file("${path.module}/files/iot_policy.json")
}

# aws_iot_policy_attachment
resource "aws_iot_policy_attachment" "att" {
  policy = aws_iot_policy.pubsub.name
  target = aws_iot_certificate.cert.arn
}

# aws_iot_thing temp_sensor
resource "aws_iot_thing" "temp_sensor" {
  name = "temp_sensor"
}

# aws_iot_thing_principal_attachment
resource "aws_iot_thing_principal_attachment" "att" {
  principal = aws_iot_certificate.cert.arn
  thing     = aws_iot_thing.temp_sensor.name
}

# aws_iot_endpoint
data "aws_iot_endpoint" "iot_endpoint" {
  endpoint_type = "iot:Data-ATS"
}

# aws_iot_topic_rule rule for sending invalid data to DynamoDB
resource "aws_iot_topic_rule" "temperature_rule" {
  name = "TemperatureRule"
  description = "Rule to insert messages into DynamoDB"
  enabled = true
  sql = "SELECT *  FROM 'sensor/temperature/+' where temperature >= 40"
  sql_version = "2016-03-23"

  dynamodbv2 {
    role_arn = aws_iam_role.iot_role.arn
    put_item {
      table_name = aws_dynamodb_table.Temperature_table.name
    }
  }
}


# aws_iot_topic_rule rule for sending valid data to Timestream
resource "aws_iot_topic_rule" "temperaturesensor_rule" {
  name        = "temperaturesensor_rule"
  description = "Example rule"
  enabled     = true
  sql         = "SELECT * FROM 'sensor/temperature/+'"
  sql_version = "2016-03-23"

  timestream {
    role_arn = aws_iam_role.iot_role.arn
    database_name = aws_timestreamwrite_database.iot.database_name
    table_name = aws_timestreamwrite_table.temperaturesensor.table_name
    dimension {
      name  = "sensor_id"
      value = "$${sensor_id}"
    }

    dimension {
      name  = "temperature"
      value = "$${temperature}"
    }

    dimension {
      name  = "zone_id"
      value = "$${zone_id}"
    }

    timestamp {
      unit  = "MILLISECONDS"
      value = "$${timestamp()}"
    }
  }


}

###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iot_logging_options" "logging_option" {
#  default_log_level = "WARN"
#  role_arn          = aws_iam_role.iot_role.arn
#}
